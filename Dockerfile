FROM debian:latest

RUN \
    apt-get update && \
    apt-get dist-upgrade --yes --no-install-recommends --no-install-suggests && \
    apt-get install --yes --no-install-recommends --no-install-suggests \
            podman \
            && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
